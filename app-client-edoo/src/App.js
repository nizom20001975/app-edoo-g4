import React from 'react';
import Navigation from "./components/Navigation";
import Footer from "./components/Footer";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Route, Switch} from "react-router-dom";
import Region from "./pages/region/region";
import Country from "./pages/country/country";
import District from "./pages/district/district";
import Todo from "./pages/todo/Todo";
import Login from "./pages/auth/login";
import Register from "./pages/auth/register";

function App() {
    return (
        <div>
            <ToastContainer/>
            <Navigation/>

            <Switch>
                <Route path="/region" component={Region}/>
                <Route path="/country" component={Country}/>
                <Route path="/district" component={District}/>
                <Route path="/todo" component={Todo}/>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Register}/>

            </Switch>
            <Footer/>
        </div>
    );
}

export default App;
