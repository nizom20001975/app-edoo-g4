import React, {Component} from 'react';
import uzgarmaslar from '../constants/constants'
import {Collapse, Nav, Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem, NavLink} from 'reactstrap'
import {toast} from "react-toastify";

class Navigation extends Component {

    render() {
        const {TOKEN_NAME} = uzgarmaslar;

        const logout = () => {
            toast.success("Tizimdan chiqildi");
            localStorage.removeItem(uzgarmaslar.TOKEN_NAME);
            window.location.replace("/");
        };


        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Home</NavbarBrand>
                    <NavbarToggler/>
                    <Collapse navbar>
                        <Nav className="mr-auto" navbar>
                            {localStorage.getItem(TOKEN_NAME) ?
                                <div style={{display: 'flex'}}>
                                    <NavItem>
                                        <NavLink href="/region">Regions</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/country">Country</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/district">District</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/todo">Todo</NavLink>
                                    </NavItem>
                                </div> :
                                ''
                            }

                        </Nav>
                        <NavbarText>
                            {!localStorage.getItem(TOKEN_NAME) ?
                                <NavLink href="/login">Kirish</NavLink> :
                                <NavLink onClick={logout}>Chiqish</NavLink>}
                        </NavbarText>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

Navigation.propTypes = {};

export default Navigation;
