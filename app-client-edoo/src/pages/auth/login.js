import React, {Component} from 'react';
import {Button, Col, Container, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from "axios";
import constants from "../../constants/constants";
import {toast} from "react-toastify";
import {getDistricts} from "../../service/service";

class Login extends Component {
    state = {
        response: {},
        checked: false
    };

    componentDidMount() {
        if (localStorage.getItem(constants.TOKEN_NAME)) {
            window.location.replace("/")
        }
    }

    render() {
        const tekshirish = (e, v) => {
            axios.post(`${constants.API_PREFIX}auth/checkUser`, v)
                .then(res => {
                    if (res.status === 200) {
                        this.setState({response: res.data, checked: true})
                    }
                }).catch(err => toast.error("Parol yoki passport ma'lumotlari xato"))
        };

        const kirish = (e, v) => {
            axios.post(constants.API_PREFIX + 'auth/login', this.state.response)
                .then(res => {
                    if (res.status === 200) {
                        localStorage.setItem(constants.TOKEN_NAME, res.data);
                        window.location.replace("/");
                        toast.success("Xush kelibsiz!!!");
                    }
                }).catch(err => toast.error("Tasdiqlash kodi xato!!!"))
        }

        return (
            <div>
                <Container>
                    {!this.state.checked ?
                        <Row>
                            <Col md={{size: 6, offset: 3}}>
                                <AvForm onValidSubmit={tekshirish}>
                                    <AvField name="passportSerial" label="Enter passport serial" required/>
                                    <AvField name="passportNumber" label="Enter passport number" required/>
                                    <AvField name="password" type="password" label="Enter password" required/>
                                    <button type="button" onClick={function () {
                                        window.location.replace("/register")
                                    }} color="primary">I have already account</button>
                                    <Button color="success">Login</Button>
                                </AvForm>
                            </Col>
                        </Row> :
                        <Row>
                            <Col md={{size: 6, offset: 3}}>
                                <Row>
                                    <Col>
                                        <h4>Sizning
                                            {this.state.response.phoneNumber.substring(0, 6)}*****{this.state.response.phoneNumber.substring(11)} ga
                                            tasdiqlash kodi yuborildi. Iltimos ushbu kodni kiriting </h4>
                                    </Col>
                                </Row>
                                <AvForm onValidSubmit={kirish}>
                                    <AvField name="code" required/>
                                    <Button color="success">Send</Button>
                                </AvForm>
                            </Col>
                        </Row>
                    }
                </Container>
            </div>
        );
    }
}

Login.propTypes = {};

export default Login;
