import React, {Component} from 'react';
import {Button, Col, Container, Row} from 'reactstrap'
import {AvField, AvForm} from 'availity-reactstrap-validation';
import axios from "axios";
import constants from "../../constants/constants";
import {toast} from "react-toastify";

class Register extends Component {
    state = {
        regions: [],
        districts: [],
        awares: [],
        phoneNumbers: ['ketmon']
    };

    componentDidMount() {
        if (localStorage.getItem(constants.TOKEN_NAME)) {
            window.location.replace("/");
        }
        this.getRegions();
        axios.get(`${constants.API_PREFIX}aware`).then(res => {
                console.log(res)
                this.setState({awares: res.data._embedded.list})
            }
        )
    }

    getRegions = () => {
        axios.get("http://localhost/api/region")
            .then(res => {
                    let regions = res.data._embedded.list;
                    this.setState({regions: regions})
                }
            )
    };

    render() {

        const getDistrictsByRegion = (e) => {
            axios.get(constants.API_PREFIX + "district/search/byRegion?regionId=" + e.target.value)
                .then(res => {
                    this.setState({districts: res.data._embedded.list})
                })
        };

        const addRow = (i) => {
            const aa = require('uuid/v4');
            let inputs = this.state.phoneNumbers;
            inputs.splice(i + 1, 0, aa())
            this.setState({phoneNumbers: inputs});
        };

        const removeRow = (i) => {
            let inputs = this.state.phoneNumbers;
            inputs.splice(i, 1);
            this.setState({phoneNumbers: inputs})
        };

        const registerUser = (e, v) => {
            console.log(v);
            let boltalar = [];
            for (let k in v) {
                if (k.includes("phoneNumbers")) {
                    boltalar.push(v[k])
                }
            }
            let contactlar = {
                districtId: v.districtId,
                address: v.address,
                email: v.email,
                fax: v.fax,
                phoneNumbers: boltalar
            };
            console.log(contactlar);
            v.reqContact = contactlar;
            console.log(v);
            axios.post(`${constants.API_PREFIX}auth/register`, v).then(res => {
                    console.log(res);
                    if (res.status === 201) {
                        toast.success(res.data.message);
                        localStorage.setItem(constants.TOKEN_NAME, res.data.object);
                        window.location.replace("/");
                    }
                }
            ).catch(rr => {
                console.log(rr);
                toast.error(rr.message)
            })
        }

        return (
            <div>
                <Container>
                    <AvForm onValidSubmit={registerUser}>
                        <Row>

                            <Col md={{size: 4, offset: 2}}>

                                <AvField name="firstName" label="First name" placeholder="Enter first name" required/>
                                <AvField name="middleName" label="middle name" placeholder="Enter middle name"/>
                                <AvField name="passportNumber" label="passportNumber" placeholder="Enter passportNumber"
                                         required/>
                                <AvField name="phoneNumber" label="phoneNumber" placeholder="Enter phoneNumber"
                                         required/>
                                <AvField type="password" name="prePassword" label="prePassword"
                                         placeholder="Enter prePassword" required/>
                                <AvField type="select" name="districtId" label="District" placeholder="Enter District"
                                         required>
                                    <option value="0">Districtni kiriting</option>
                                    {this.state.districts.map(item =>
                                        <option key={item.id} value={item.id}>{item.nameUz}</option>
                                    )}
                                </AvField>
                                <AvField name="email" label="email" placeholder="Enter email" required/>
                                <AvField type="select" name="awareId" label="awareId" placeholder="Enter awareId"
                                         required>
                                    <option value="0">Awareni tanlang</option>
                                    {this.state.awares.map(item =>
                                        <option key={item.id} value={item.id}>{item.nameUz}</option>
                                    )}
                                </AvField>
                            </Col>
                            <Col md={{size: 4, offset: 2}}>
                                <AvField name="lastName" label="last name" placeholder="Enter last name" required/>
                                <AvField name="passportSerial" className="text-uppercase" label="passportSerial"
                                         placeholder="Enter passportSerial"
                                         required/>
                                <AvField type="date" name="birthDate" label="birthDate" placeholder="Enter birthDate"
                                         required/>
                                <AvField type="password" name="password" label="password" placeholder="Enter password"
                                         required/>
                                <AvField onChange={getDistrictsByRegion} type="select" name="regionId" label="Region"
                                         placeholder="Enter Region" required>
                                    <option value="0">Regionni kiriting</option>
                                    {this.state.regions.map(item =>
                                        <option key={item.id} value={item.id}>{item.nameUz}</option>
                                    )}
                                </AvField>
                                <AvField name="address" label="address" placeholder="Enter address" required/>
                                <AvField name="fax" label="fax" placeholder="Enter fax" required/>
                            </Col>
                        </Row>
                        {this.state.phoneNumbers.map((item, i) =>
                            <Row className="mt-2" key={item}>
                                <Col md={{size: 6, offset: 3}}>
                                    <AvField name={`phoneNumbers/${i}`} placeholder="Enter phoneNumber"/>
                                </Col>
                                <Col md="1">
                                    {this.state.phoneNumbers.length < 2 ? '' :
                                        <Button type="button" color="danger" onClick={() => removeRow(i)}>-</Button>}
                                </Col>
                                <Col md="1">
                                    <Button type="button" color="success" onClick={() => addRow(i)}>+</Button>
                                </Col>
                            </Row>
                        )}
                        <Button color="primary">Register</Button>
                    </AvForm>
                </Container>
            </div>
        );
    }
}

Register.propTypes = {};

export default Register;
