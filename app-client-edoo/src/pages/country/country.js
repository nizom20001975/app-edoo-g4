import React, {Component} from 'react';
import {Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from 'reactstrap';

class Country extends Component {
    state = {
        countries: [],
        showModal: false,
        countryName: '',
        currentCountry: ''
    };


    render() {

        const openModal = (huja) => {
            this.setState({showModal: !this.state.showModal, currentCountry: huja})
        };

        const saveCountry = () => {
            let name = this.state.countryName;
            let davlatlar = this.state.countries;
            let hozirgiDavlat = this.state.currentCountry;
            let newDavlat = '';
            if (name) {
                if (hozirgiDavlat) {
                    newDavlat = {id: hozirgiDavlat.id, name: name};
                    let index = davlatlar.indexOf(hozirgiDavlat);
                    davlatlar.splice(index, 1, newDavlat);
                } else {
                    let maxId = davlatlar.length === 0 ? 1 : davlatlar[davlatlar.length - 1].id + 1;
                    newDavlat = {id: maxId, name: name};
                    davlatlar.push(newDavlat);
                }
                this.setState({countries: davlatlar, showModal: false, countryName: ''});
            }
        };

        const getCountryName = (e) => {
            this.setState({countryName: e.target.value});
        };

        const openDeleteModal = (davlat) => {
            let javob = window.confirm(davlat.name + 'ni uchirasizmi?');
            if (javob) {
                let davlatlar = this.state.countries;
                davlatlar.splice(davlatlar.indexOf(davlat), 1);
                this.setState({countries: davlatlar});
            }
        };


        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <h1>Country list</h1>
                            <Button color="danger" onClick={() => openModal('')}>Add country</Button>
                            <Table className="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Tr</th>
                                    <th>Name</th>
                                    <th colSpan="2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.countries.map((davlat, index) =>
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{davlat.name}</td>
                                        <td>
                                            <Button color="warning" onClick={() => openModal(davlat)}>Edit</Button>
                                        </td>
                                        <td>
                                            <Button color="danger"
                                                    onClick={() => openDeleteModal(davlat)}>Delete</Button>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            </Table>
                        </div>
                    </div>
                </div>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{this.state.currentCountry ? 'Edit country' : 'Add Country'}</ModalHeader>
                    <ModalBody>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" id="name" defaultValue={this.state.currentCountry.name}
                               onChange={getCountryName}
                               placeholder="Enter country name"/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={saveCountry}>Save</Button>{' '}
                        <Button color="secondary" onClick={openModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Country.propTypes = {};

export default Country;
