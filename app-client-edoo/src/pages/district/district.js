import React, {Component} from 'react';
import axios from "axios";
import {Button, Container, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {toast} from "react-toastify";

class District extends Component {
    state = {
        districts: [],
        showModal: false,
        regions: []
    };

    componentDidMount() {
        this.getDistrict();
        // getDistricts();
        axios.get("http://localhost/api/region")
            .then(res => this.setState({regions: res.data._embedded.list}));
    }

    getDistrict = () => {
        axios.get("http://localhost/api/district")
            .then(res => this.setState({districts: res.data._embedded.list}));
    };

    render() {

        const openModal = () => {
            this.setState({showModal: !this.state.showModal})
        };

        const saveDistrict = (event, errors, value) => {
            if (errors.length === 0) {
                axios.post("http://localhost/api/district", value)
                    .then(res => {
                        if (res.status === 201) {
                            this.getDistrict();
                            toast.success("District saved");
                            this.setState({showModal: false})
                        } else {
                            toast.error("Error in saving")
                        }
                    });
            }
        };


        return (
            <div>
                <Container>
                    <h1 className="text-center">District list</h1>
                    <Button onClick={() => openModal('')} color="primary">Add district</Button>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/r</th>
                            <th>NomiUz</th>
                            <th>NomiRu</th>
                            <th>NomiEn</th>
                            <th>Region nomi</th>
                            <th colSpan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.districts.map((district, i) =>
                            <tr key={district.id}>
                                <td>{i + 1}</td>
                                <td>{district.nameUz}</td>
                                <td>{district.nameRu}</td>
                                <td>{district.nameEn}</td>
                                <td>{district.region.nameUz}</td>
                                <td>
                                    {/*<button className="btn btn-warning" onClick={() => openModal(region)}>Edit</button>*/}
                                </td>
                                <td>
                                    {/*<button className="btn btn-danger" onClick={() => openDeleteModal(region)}>Delete</button>*/}
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                </Container>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        Add district
                    </ModalHeader>
                    <ModalBody>
                        <AvForm onSubmit={saveDistrict}>
                            <AvField name="nameUz" placeholder="Enter name Uz" required/>
                            <AvField name="nameRu" placeholder="Enter name Ru" required/>
                            <AvField name="nameEn" placeholder="Enter name En" required/>
                            <AvField name="region" type="select" value="0" required>
                                <option value="0" disabled={true}>Viloyatni tanlang</option>
                                {this.state.regions.map(item =>
                                    <option key={item.id} value={"/" + item.id}>{item.nameUz}</option>
                                )}
                            </AvField>
                            <Button type="button" color="danger" onClick={() => openModal('')}>Cancel</Button>
                            <Button color="success">Save</Button>
                        </AvForm>

                    </ModalBody>
                    <ModalFooter>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

District.propTypes = {};

export default District;
