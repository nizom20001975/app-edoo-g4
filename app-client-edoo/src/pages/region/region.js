import React, {Component} from 'react';
import {Button, Container, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import axios from 'axios';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {toast} from "react-toastify";

class Region extends Component {
    state = {
        regions: [],
        visibleModal: false,
        currentRegion: '',
        visibleModalDelete: false
    };


    componentDidMount() {
        this.getRegions();
    }

    getRegions = () => {
        axios.get("http://localhost/api/region")
            .then(res => {
                    let regions = res.data._embedded.list;
                    this.setState({regions: regions})
                }
            )
    };


    render() {
        const openModal = (region) => {
            this.setState({
                visibleModal: !this.state.visibleModal,
                currentRegion: region
            })
        };

        const saveRegion = (e, v) => {
            let currentRegion = this.state.currentRegion;
            axios({
                url: "http://localhost/api/region" + (currentRegion ? '/' + currentRegion.id : ''),
                method: currentRegion ? 'PUT' : 'POST',
                data: v
            }).then(res => {
                    if (res) {
                        toast.success(currentRegion ? "Region tahrirlandi " : "Region added");
                        this.getRegions();
                        this.setState({visibleModal: false})
                    }
                }
            ).catch(res =>
                toast.error("This name already exist")
            )
        };

        const openDeleteModal = (region) => {
            this.setState({
                visibleModalDelete: !this.state.visibleModalDelete,
                currentRegion: region
            })
        };

        const deleteRegion = () => {
            console.log(this.state.currentRegion)
            axios.delete('http://localhost/api/region/' + this.state.currentRegion.id)
                .then(res => {
                        if (res.status === 204) {
                            toast.success(this.state.currentRegion.nameUz + ' deleted');
                            this.setState({visibleModalDelete: false})
                            this.getRegions();
                        }
                    }
                ).catch(res => {
                toast.error(this.state.currentRegion.nameUz + ' can not deleting');
                this.setState({visibleModalDelete: false});
            })
        };

        return (
            <div>
                <Container>
                    <h1 className="text-center">Region list</h1>
                    <Button onClick={() => openModal('')} color="primary">Add region</Button>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/r</th>
                            <th>NomiUz</th>
                            <th>NomiRu</th>
                            <th>NomiEn</th>
                            <th colSpan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.regions.map((region, i) =>
                            <tr key={region.id}>
                                <td>{i + 1}</td>
                                <td>{region.nameUz}</td>
                                <td>{region.nameRu}</td>
                                <td>{region.nameEn}</td>
                                <td>
                                    <button className="btn btn-warning" onClick={() => openModal(region)}>Edit</button>
                                </td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => openDeleteModal(region)}>Delete
                                    </button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                </Container>

                <Modal isOpen={this.state.visibleModal}>
                    <ModalHeader>
                        Add Region
                    </ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={saveRegion}>
                            <AvField defaultValue={this.state.currentRegion.nameUz} name="nameUz" label="Region name uz"
                                     placeholder="Enter name uz" required/>
                            <AvField defaultValue={this.state.currentRegion.nameRu} name="nameRu" label="Region name Ru"
                                     placeholder="Enter name Ru" required/>
                            <AvField defaultValue={this.state.currentRegion.nameEn} name="nameEn" label="Region name En"
                                     placeholder="Enter name En" required/>
                            <Button type="button" onClick={openModal} color="danger">Cancel</Button>
                            <Button color="success">Save</Button>
                        </AvForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.visibleModalDelete}>
                    <ModalHeader>
                        {'Are you sure delete ' + this.state.currentRegion.nameUz + '?'}
                    </ModalHeader>
                    <ModalFooter>
                        <Button color="primary" onClick={openDeleteModal}>No</Button>
                        <Button color="danger" onClick={deleteRegion}>Yes</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Region.propTypes = {};

export default Region;
