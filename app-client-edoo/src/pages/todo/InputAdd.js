import React, {Component} from 'react';
import {Button, Col, Container, Input, Row} from "reactstrap";

class InputAdd extends Component {
    state = {
        inputs: [{id: 1}],
    };


    render() {
        const addInput = (i) => {
            let ketmon = this.state.inputs;
            // let maxId = ketmon[0].id;
            // for (let j = 0; j < ketmon.length; j++) {
            //     maxId = Math.max(maxId, ketmon[j].id);
            // }
            const aa = require('uuid/v4');
            ketmon.splice(i + 1, 0, {id: aa()});
            this.setState({inputs: ketmon})
        };


        const removeInput = (i) => {
            console.log(i);
            let inputs = this.state.inputs;
            inputs.splice(i, 1);
            this.setState({inputs})
        };
        return (
            <div>
                <Container>
                    <Row>
                        Inputlar qatori
                    </Row>

                    {this.state.inputs.map((item, i) =>
                        <Row key={item.id}>
                            <Col md="4">
                                <Input name={"name" + i}/>
                            </Col>
                            {this.state.inputs.length > 1 ? <Col md="1">
                                <Button color="danger" onClick={() => removeInput(i)}>-</Button>
                            </Col> : <Col md="1"></Col>}
                            <Col md="1">
                                <Button color="success" onClick={() => addInput(i)}>+</Button>
                            </Col>

                        </Row>
                    )}
                </Container>
            </div>
        );
    }
}

InputAdd.propTypes = {};

export default InputAdd;
