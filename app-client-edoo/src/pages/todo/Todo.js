import React, {Component} from 'react';
import {Button, Col, Container, Input, Row} from "reactstrap";
import TodoItem from "./TodoItem";
import InputAdd from "./InputAdd";

class Todo extends Component {
    state = {
        tasks: [],
        showInput: false,
        nameLocal: 'taskLocal'
    };

    componentDidMount() {
        let tasks = localStorage.getItem(this.state.nameLocal);
        tasks = JSON.parse(tasks);
        if (tasks) {
            this.setState({tasks});
        }
    }


    render() {

        const writeLocalStorage = () => {
            localStorage.setItem(this.state.nameLocal, JSON.stringify(this.state.tasks));
        };

        const changeCompleted = (item) => {
            let tasks = this.state.tasks;
            let index = tasks.indexOf(item);
            tasks[index].completed = !item.completed;
            this.setState({tasks});
            writeLocalStorage();
        };


        const addTask = () => {
            this.setState({showInput: !this.state.showInput})
        };

        const saveTask = () => {
            let name = document.getElementById("name").value;
            let tasks = this.state.tasks;
            let newTask = {id: tasks.length === 0 ? 1 : tasks[tasks.length - 1].id + 1, name, completed: false};
            tasks.push(newTask);
            this.setState({tasks, showInput: false});
            writeLocalStorage();
        };

        return (
            <div>
                <InputAdd/>
                <Container style={{backgroundColor: 'cyan'}}>
                    <Row>
                        <Col md="2">
                            <Button color="primary" onClick={addTask}>
                                Add task
                            </Button>
                        </Col>
                        <Col md="4">
                            <h1 className="text-center">TODO list</h1>
                        </Col>
                        <Col md="2">
                            <Row>
                                <Col>
                                    Completed
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    {this.state.tasks.filter(item => item.completed).length}

                                </Col>
                            </Row>

                        </Col>
                        <Col md="2">
                            <Row>
                                <Col>
                                    Progress
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    {this.state.tasks.filter(item => !item.completed).length}
                                </Col>
                            </Row>
                        </Col>
                        <Col md="2">
                            <Row>
                                <Col>
                                    All
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    {this.state.tasks.length}
                                </Col>
                            </Row>
                        </Col>
                    </Row>

                    {this.state.showInput ?
                        <Row>
                            <Col md="4">
                                <Input name="name" id="name"/>
                            </Col>
                            <Col md="3">
                                <Button onClick={saveTask}>Save</Button>
                                <Button onClick={addTask}>Cancel</Button>
                            </Col>
                        </Row> : ''
                    }


                    {this.state.tasks.length === 0 ?
                        <Row>
                            <Col>
                                <h5>Tasklar yo'q</h5>
                            </Col>
                        </Row> :
                        this.state.tasks.map(bittaTask =>
                            <Row key={bittaTask.id}>
                                <Col md={{size: 3, offset: 2}}>
                                    <TodoItem tesha={bittaTask} changeCompleted={changeCompleted}/>
                                </Col>
                            </Row>
                        )}

                </Container>
            </div>
        );
    }
}

Todo.propTypes = {};

export default Todo;
