import React, {Component} from 'react';
import {Card, CardBody, CardTitle, Input} from "reactstrap";

class TodoItem extends Component {
    render() {
        const {tesha, changeCompleted} = this.props;
        return (
            <div>
                <Card style={{backgroundColor: `${tesha.completed ? 'green' : 'red'}`}}>
                    <CardTitle>
                        <Input type="checkbox" onChange={() => changeCompleted(tesha)} checked={tesha.completed}
                               name={tesha ? tesha.id : "gfsdajk"}/>
                    </CardTitle>
                    <CardBody>
                        {tesha.name}
                    </CardBody>
                </Card>
            </div>
        );
    }
}

TodoItem.propTypes = {};

export default TodoItem;
