import axios from "axios";
import constants from "../constants/constants";

export const getDistricts = getDistricts => () => {
    return axios.get(constants.API_PREFIX + 'district');
};