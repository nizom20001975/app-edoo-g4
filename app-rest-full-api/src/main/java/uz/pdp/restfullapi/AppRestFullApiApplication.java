package uz.pdp.restfullapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRestFullApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppRestFullApiApplication.class, args);
    }


}
