package uz.pdp.restfullapi.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uz.pdp.restfullapi.entity.EduPayment;
import uz.pdp.restfullapi.entity.EduPaymentLog;
import uz.pdp.restfullapi.entity.enums.OperationEnum;
import uz.pdp.restfullapi.repository.EduPaymentLogRepository;

import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

@Component
public class EduPaymentListener {
    private final EduPaymentLogRepository eduPaymentLogRepository;

    public EduPaymentListener(@Lazy EduPaymentLogRepository eduPaymentLogRepository) {
        this.eduPaymentLogRepository = eduPaymentLogRepository;
    }


    @PreUpdate
    public void editEduPayment(Object object) {
        EduPayment eduPayment = (EduPayment) object;
        saveLog(eduPayment,OperationEnum.UPDATE);
    }

    @PreRemove
    public void deleteEduPayment(Object object) {
        EduPayment eduPayment = (EduPayment) object;
        saveLog(eduPayment,OperationEnum.DELETE);
    }

    public void saveLog(EduPayment eduPayment,OperationEnum operationEnum){
        EduPaymentLog eduPaymentLog = new EduPaymentLog();
        eduPaymentLog.setEduPaymentId(eduPayment.getId());
        eduPaymentLog.setEduCenter(eduPayment.getEduCenter());
        eduPaymentLog.setPaySum(eduPayment.getPaySum());
        eduPaymentLog.setPayDate(eduPayment.getPayDate());
        eduPaymentLog.setPayType(eduPayment.getPayType());
        eduPaymentLog.setOperation(operationEnum);
        eduPaymentLogRepository.save(eduPaymentLog);
    }
}
