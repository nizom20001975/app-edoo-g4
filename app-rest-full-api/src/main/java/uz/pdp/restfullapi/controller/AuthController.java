package uz.pdp.restfullapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqLogin;
import uz.pdp.restfullapi.payload.ReqRegister;
import uz.pdp.restfullapi.payload.ResLogin;
import uz.pdp.restfullapi.repository.UserRepository;
import uz.pdp.restfullapi.security.JwtTokenProvider;
import uz.pdp.restfullapi.service.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody ReqRegister reqRegister) {
        ApiResponse response = authService.register(reqRegister);
        ResLogin resLogin = new ResLogin(reqRegister.getPassportSerial(), reqRegister.getPassportNumber(), reqRegister.getPhoneNumber());
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT)
                .body(response.isSuccess() ? new ApiResponse(response.getMessage(), true, generateToken(resLogin)) : response);
    }

    @PostMapping("/checkUser")
    public HttpEntity<?> checkUser(@RequestBody ReqLogin reqLogin) {
        String username = reqLogin.getPassportSerial() + "@@" + reqLogin.getPassportNumber();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, reqLogin.getPassword())
        );
        User user = (User) authentication.getPrincipal();
        return ResponseEntity.ok(new ResLogin(user.getPassportSerial(), user.getPassportNumber(), user.getPhoneNumber()));
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ResLogin request) {
        return ResponseEntity.ok(generateToken(request));
    }

    private String generateToken(ResLogin request) {
        User user = userRepository.findByPassportSerialAndPassportNumber(request.getPassportSerial(), request.getPassportNumber()).orElseThrow(() -> new UsernameNotFoundException("getUser"));
        return jwtTokenProvider.generateToken(user.getId());
    }


}
