package uz.pdp.restfullapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqCompanyInfo;
import uz.pdp.restfullapi.service.CompanyInfoService;

@RestController
@RequestMapping("/api/companyInfo")
public class CompanyInfoController {

    @Autowired
    CompanyInfoService companyInfoService;

    @PostMapping
    public HttpEntity<?> addCompanyInfo(@RequestBody ReqCompanyInfo reqCompanyInfo) {
        ApiResponse apiResponse = companyInfoService.addCompanyInfo(reqCompanyInfo);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }
}
