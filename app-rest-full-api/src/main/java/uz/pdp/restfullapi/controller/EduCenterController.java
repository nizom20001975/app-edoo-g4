package uz.pdp.restfullapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqEduCenter;
import uz.pdp.restfullapi.payload.ResEduCenter;
import uz.pdp.restfullapi.payload.ResPageable;
import uz.pdp.restfullapi.security.CurrentUser;
import uz.pdp.restfullapi.service.EduCenterService;
import uz.pdp.restfullapi.utils.AppConstant;

import java.util.UUID;

@RestController
@RequestMapping("/api/eduCenter")
public class EduCenterController {
    @Autowired
    EduCenterService eduCenterService;

    @PostMapping
    public HttpEntity<?> registerEdu(@RequestBody ReqEduCenter reqEduCenter, @CurrentUser User user) {
        ApiResponse apiResponse = eduCenterService.registerEdu(reqEduCenter, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editEdu(@PathVariable UUID id, @RequestBody ReqEduCenter reqEduCenter, @CurrentUser User user) {
        ApiResponse apiResponse = eduCenterService.editEdu(id, reqEduCenter, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PatchMapping("/{id}")
    public HttpEntity<?> changeEnabled(@PathVariable UUID id, @CurrentUser User user) {
        ApiResponse apiResponse = eduCenterService.changeEnabled(id, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getEduCenterOnlyAdminAndDirectorAndManager(@PathVariable UUID id, @CurrentUser User user) {
        ResEduCenter resEduCenter = eduCenterService.getEduCenterOnlyAdminAndDirectorAndManager(id, user);
        return ResponseEntity.ok(resEduCenter);
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @GetMapping
    public HttpEntity<?> getEduCenters(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size,
                                       @CurrentUser User user) {
        ResPageable resPageable = eduCenterService.getEduCentersForAdmin(page, size,user);
        return ResponseEntity.ok(resPageable);
    }
}
