package uz.pdp.restfullapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqEduPayment;
import uz.pdp.restfullapi.repository.EduPaymentRepository;
import uz.pdp.restfullapi.service.EduPaymentService;

import java.util.UUID;

@RestController
@RequestMapping("/api/eduPayment")
public class EduPaymentController {

    @Autowired
    EduPaymentService eduPaymentService;
    @Autowired
    EduPaymentRepository eduPaymentRepository;

    @PostMapping
    public HttpEntity<?> addEduPayment(@RequestBody ReqEduPayment reqEduPayment) {
        ApiResponse apiResponse = eduPaymentService.addEduPayment(reqEduPayment);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editEduPayment(@PathVariable UUID id, @RequestBody ReqEduPayment reqEduPayment) {
        ApiResponse apiResponse = eduPaymentService.editEduPayment(id,reqEduPayment);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteEduPayment(@PathVariable UUID id) {
        eduPaymentRepository.deleteById(id);
        return ResponseEntity.ok("deleted");
    }
}
