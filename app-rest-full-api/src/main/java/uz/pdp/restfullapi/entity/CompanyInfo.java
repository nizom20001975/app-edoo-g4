package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CompanyInfo extends AbsEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String brandName;

    @OneToMany(mappedBy = "companyInfo")
    private List<AwareCompany> awareCompanies;

    @OneToOne
    private Contact contact;
    @Column(columnDefinition = "text")
    private String definition;

}
