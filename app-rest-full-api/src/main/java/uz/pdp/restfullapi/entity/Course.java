package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsNameEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Course extends AbsNameEntity {

    private Integer durationCourse;
    @ManyToOne(optional = false)
    private DurationType durationTypeCourse;

    private Integer durationLesson;
    @ManyToOne(optional = false)
    private DurationType durationTypeLesson;

    @ManyToOne(optional = false)
    private Direction direction;

    @Column(columnDefinition = "text")
    private String definition;
}
