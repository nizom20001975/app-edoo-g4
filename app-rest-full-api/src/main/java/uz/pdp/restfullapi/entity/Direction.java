package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nameUz", "edu_center_id"}),
        @UniqueConstraint(columnNames = {"nameRu", "edu_center_id"}),
        @UniqueConstraint(columnNames = {"nameEn", "edu_center_id"})
})
public class Direction extends AbsNameEntity {
    @OneToMany(mappedBy = "direction", cascade = CascadeType.ALL)
    private List<Course> courses;

    @ManyToOne(optional = false)
    private EduCenter eduCenter;
}
