package uz.pdp.restfullapi.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.restfullapi.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class DurationType extends AbsNameEntity {

}
