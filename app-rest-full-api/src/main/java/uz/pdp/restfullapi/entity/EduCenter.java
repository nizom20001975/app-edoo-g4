package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class EduCenter extends AbsEntity {
    @Column(unique = true, nullable = false)
    private String name;//yuridik nomi
    @Column(unique = true, nullable = false)
    private String brand;//brand nomi reklam uchun
    @Column(unique = true, nullable = false, length = 9)
    private String tin;
    private String accountNumber;

    @Column(nullable = false)
    private Date foundedDate;

    @OneToOne(optional = false)
    private Contact contact;

    @ManyToOne(optional = false)
    private BusinessType businessType;

    @Column(unique = true, nullable = false)
    private String licenseNumber;

    @Column(nullable = false)
    private Date licenseExpire;

    @ManyToMany
    @JoinTable(name = "edu_center_edu_type",
            joinColumns = {@JoinColumn(name = "edu_center_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "edu_type_id", nullable = false)}
    )
    private List<EduType> eduTypes;

    @OneToOne
    private Attachment logo;

    @OneToOne(optional = false)
    private Attachment license;

    @OneToMany
    private List<Attachment> gallery;

    @ManyToOne(optional = false)
    private Tariff tariff;

    private boolean enabled;
}
