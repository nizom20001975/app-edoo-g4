package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.component.EduPaymentListener;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(EduPaymentListener.class)
public class EduPayment extends AbsEntity {

    @Column(nullable = false)
    private double paySum;

    private Timestamp payDate = new Timestamp(new Date().getTime());

    @ManyToOne(optional = false)
    private PayType payType;

    @ManyToOne(optional = false)
    private EduCenter eduCenter;


}
