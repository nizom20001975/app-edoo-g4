package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.enums.OperationEnum;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class EduPaymentLog extends AbsEntity {

    private UUID eduPaymentId;

    @Column(nullable = false)
    private double paySum;

    private Timestamp payDate;

    @ManyToOne(optional = false)
    private PayType payType;

    @ManyToOne(optional = false)
    private EduCenter eduCenter;

    @Enumerated(EnumType.STRING)
    private OperationEnum operation;

}
