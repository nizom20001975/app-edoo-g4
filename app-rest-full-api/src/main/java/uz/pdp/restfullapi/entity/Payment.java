package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment extends AbsEntity {
    @ManyToOne(optional = false)
    private PayType payType;

    private double paySum;

    @ManyToOne(optional = false)
    private Student student;

    private Timestamp payDate = new Timestamp(new Date().getTime());
}
