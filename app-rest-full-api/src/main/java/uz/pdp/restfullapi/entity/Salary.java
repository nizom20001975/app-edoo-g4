package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Salary extends AbsEntity {

    private double amount;
    private Timestamp givenDate;

    @ManyToOne
    private Staff staff;

    @ManyToOne
    private Teacher teacher;

    @ManyToOne(optional = false)
    private CashType cashType;

}
