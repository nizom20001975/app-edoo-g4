package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsNameEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Tariff extends AbsNameEntity {//TARIFFLARNI QAYD ETISH TABLE
    @Column(nullable = false)
    private Integer groupAmount;
    @Column(nullable = false)
    private double price;

    private boolean active;
}
