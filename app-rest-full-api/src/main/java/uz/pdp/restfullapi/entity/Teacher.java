package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Teacher extends AbsEntity {

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne(optional = false)
    private EduCenter eduCenter;

    private String specialization;

    @ManyToOne(optional = false)
    private GraduationType graduationType;

    private Double experience;
}
