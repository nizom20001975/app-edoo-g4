package uz.pdp.restfullapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Time;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TimeTable extends AbsEntity {
    @ManyToOne(optional = false)
    private Group group;

    @ManyToOne(optional = false)
    private Teacher teacher;

    @ElementCollection
    private List<Integer> weekDays;

    private Time startTimeLesson;

    @OneToMany(mappedBy = "timeTable")
    private List<Attendance> attendances;
}
