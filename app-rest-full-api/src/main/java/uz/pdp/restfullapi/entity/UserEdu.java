package uz.pdp.restfullapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEdu extends AbsEntity {
    @ManyToOne(optional = false)
    private EduCenter eduCenter;

    @ManyToMany
    @JoinTable(name = "user_edu_legal",
            joinColumns = {@JoinColumn(name = "user_edu_id")},
            inverseJoinColumns = {@JoinColumn(name = "legal_id")}
    )
    private Set<Legal> legals;

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private User user;
}
