package uz.pdp.restfullapi.entity.enums;

public enum LegalName {
    LEGAL_STUDENT,
    LEGAL_TEACHER,
    LEGAL_RECEPTION,
    LEGAL_MANAGER,
    LEGAL_DIRECTOR
}
