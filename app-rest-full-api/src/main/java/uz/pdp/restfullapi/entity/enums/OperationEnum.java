package uz.pdp.restfullapi.entity.enums;


public enum OperationEnum {
    INSERT, UPDATE, DELETE
}
