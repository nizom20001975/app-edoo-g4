package uz.pdp.restfullapi.entity.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
