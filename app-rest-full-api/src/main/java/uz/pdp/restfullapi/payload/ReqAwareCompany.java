package uz.pdp.restfullapi.payload;

import lombok.Data;

@Data
public class ReqAwareCompany {
    private Integer id;//1
    private String nameUz;//dustlar
    private String nameRu;//druzya
    private String nameEn;//friend
}
