package uz.pdp.restfullapi.payload;

import lombok.Data;

import java.util.List;

@Data
public class ReqCompanyInfo {

    private String name;
    private String brandName;
    private List<ReqAwareCompany> reqAwareCompanies;
    private ReqContact reqContact;
    private String definition;
}
