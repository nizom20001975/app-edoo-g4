package uz.pdp.restfullapi.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Data
public class ReqContact {
    @NotNull
    private Integer districtId;
    private String address;
    @Email
    private String email;
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "Phone number must be 13 digits.")
    private String fax;
    private Set<String> phoneNumbers;
}
