package uz.pdp.restfullapi.payload;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class ReqEduCenter {
    private UUID id;
    private String name;//yuridik nomi
    private String brand;//brand nomi reklam uchun
    private String tin;
    private String accountNumber;

    private Date foundedDate;

    private ReqContact reqContact;

    private Integer businessTypeId;

    private String licenseNumber;

    private Date licenseExpire;

    private List<Integer> eduTypesId;

    private UUID logoId;

    private UUID licenseId;

    private List<UUID> galleryId;
}
