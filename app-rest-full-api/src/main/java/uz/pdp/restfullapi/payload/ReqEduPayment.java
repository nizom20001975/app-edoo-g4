package uz.pdp.restfullapi.payload;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Data
public class ReqEduPayment {
    private UUID eduCenterId;
    private double paySum;
    private Timestamp payDate;
    private Integer payTypeId;
}
