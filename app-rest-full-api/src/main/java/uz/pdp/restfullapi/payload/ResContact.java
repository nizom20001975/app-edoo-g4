package uz.pdp.restfullapi.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.District;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResContact {
    private District district;
    private String address;
    private String email;
    private String fax;
    private Set<String> phoneNumbers;
}
