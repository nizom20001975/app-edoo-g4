package uz.pdp.restfullapi.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.restfullapi.entity.BusinessType;
import uz.pdp.restfullapi.entity.EduType;
import uz.pdp.restfullapi.entity.Tariff;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEduCenter {
    private UUID id;
    private String name;//yuridik nomi
    private String brand;//brand nomi reklam uchun
    private String tin;
    private String accountNumber;

    private Date foundedDate;

    private ResContact contact;

    private BusinessType businessType;

    private String licenseNumber;

    private Date licenseExpire;

    private List<EduType> eduTypes;

    private UUID logoId;

    private UUID licenseId;

    private List<UUID> galleryId;

    private boolean enabled;

    private Date expireDate;

    private Tariff tariff;
}
