package uz.pdp.restfullapi.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResLogin {
    private String passportSerial;
    private String passportNumber;
    private String phoneNumber;

}
