package uz.pdp.restfullapi.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.restfullapi.entity.Aware;
import uz.pdp.restfullapi.entity.Region;

@Projection(name = "customAware",types = Aware.class)
public interface CustomAware {
    Integer getId();
    String getNameUz();
    String getNameRu();
    String getNameEn();
    String getColor();
}
