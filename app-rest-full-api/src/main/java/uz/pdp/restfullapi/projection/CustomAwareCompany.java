package uz.pdp.restfullapi.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.restfullapi.repository.AwareCompanyRepository;

@Projection(types = AwareCompanyRepository.class)
public interface CustomAwareCompany {

}
