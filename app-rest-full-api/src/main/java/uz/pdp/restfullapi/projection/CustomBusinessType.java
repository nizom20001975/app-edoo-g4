package uz.pdp.restfullapi.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.restfullapi.entity.BusinessType;

@Projection(name = "customBusinessType", types = BusinessType.class)
public interface CustomBusinessType {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();

}
