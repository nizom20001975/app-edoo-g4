package uz.pdp.restfullapi.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.restfullapi.entity.Region;

@Projection(types = Region.class)
public interface CustomRegion {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
