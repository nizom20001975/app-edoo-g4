package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.AwareCompany;

import java.util.UUID;

public interface AwareCompanyRepository extends JpaRepository<AwareCompany, UUID> {

}
