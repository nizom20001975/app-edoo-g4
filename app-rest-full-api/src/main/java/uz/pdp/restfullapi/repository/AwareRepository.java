package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.restfullapi.entity.Aware;
import uz.pdp.restfullapi.projection.CustomAware;

import java.util.List;
@CrossOrigin
@RepositoryRestResource(path = "/aware",
        collectionResourceRel = "list",
        excerptProjection = CustomAware.class)
public interface AwareRepository extends JpaRepository<Aware, Integer> {

    @RestResource(path ="/byName")
    List<Aware> findAllByNameUzContainsIgnoreCaseOrNameRuContainsIgnoreCaseOrNameEnContainsIgnoreCase(String nameUz, String nameRu, String nameEn);

}
