package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.restfullapi.entity.BusinessType;
import uz.pdp.restfullapi.projection.CustomBusinessType;

import javax.persistence.criteria.CriteriaBuilder;

@RepositoryRestResource(path = "/businessType",collectionResourceRel = "list",excerptProjection = CustomBusinessType.class)
public interface BusinessTypeRepository extends JpaRepository<BusinessType, Integer> {
}
