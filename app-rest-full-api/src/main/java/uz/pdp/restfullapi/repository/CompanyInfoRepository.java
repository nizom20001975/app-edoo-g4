package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.CompanyInfo;

public interface CompanyInfoRepository extends JpaRepository<CompanyInfo,Integer> {
}
