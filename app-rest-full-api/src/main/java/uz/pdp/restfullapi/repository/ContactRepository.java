package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.restfullapi.entity.Contact;
import uz.pdp.restfullapi.entity.District;

import java.util.UUID;

public interface ContactRepository extends JpaRepository<Contact, UUID> {
    boolean existsByEmailEqualsIgnoreCase(String email);
    boolean existsByEmailEqualsIgnoreCaseAndIdNot(String email, UUID id);
}
