package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.EduCenter;

import java.util.Optional;
import java.util.UUID;

public interface EduCenterRepository extends JpaRepository<EduCenter, UUID> {
    boolean existsByNameEqualsIgnoreCase(String name);

    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, UUID id);

    boolean existsByBrandEqualsIgnoreCase(String brand);

    boolean existsByBrandEqualsIgnoreCaseAndIdNot(String brand, UUID id);

    boolean existsByTin(String tin);

    boolean existsByTinAndIdNot(String tin, UUID id);

    Optional<EduCenter> findByIdAndEnabled(UUID id, boolean enabled);

}
