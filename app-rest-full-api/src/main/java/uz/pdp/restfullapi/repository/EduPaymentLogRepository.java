package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.EduCenter;
import uz.pdp.restfullapi.entity.EduPaymentLog;

import java.util.Optional;
import java.util.UUID;

public interface EduPaymentLogRepository extends JpaRepository<EduPaymentLog, UUID> {

}
