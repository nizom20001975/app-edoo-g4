package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.pdp.restfullapi.entity.Aware;
import uz.pdp.restfullapi.entity.EduType;
import uz.pdp.restfullapi.projection.CustomAware;

import java.util.List;

@RepositoryRestResource(path = "/eduType")
public interface EduTypeRepository extends JpaRepository<EduType, Integer> {



}
