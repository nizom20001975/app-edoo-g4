package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.Legal;
import uz.pdp.restfullapi.entity.enums.LegalName;

public interface LegalRepository extends JpaRepository<Legal, Integer> {
    Legal findByLegalName(LegalName legalName);

}
