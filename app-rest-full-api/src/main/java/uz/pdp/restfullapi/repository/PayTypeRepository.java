package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.PayType;

public interface PayTypeRepository extends JpaRepository<PayType,Integer> {
}
