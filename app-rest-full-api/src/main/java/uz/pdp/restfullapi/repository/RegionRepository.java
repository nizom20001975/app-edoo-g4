package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.restfullapi.entity.Region;
import uz.pdp.restfullapi.projection.CustomRegion;

@CrossOrigin
@RepositoryRestResource(path = "/region", excerptProjection = CustomRegion.class, collectionResourceRel = "list")
public interface RegionRepository extends JpaRepository<Region, Integer> {

}
