package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.Tariff;

public interface TariffRepository extends JpaRepository<Tariff,Integer> {
    Tariff findByPrice(double price);
}
