package uz.pdp.restfullapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.entity.UserEdu;

import java.util.List;
import java.util.UUID;

public interface UserEduRepository extends JpaRepository<UserEdu, UUID> {
}
