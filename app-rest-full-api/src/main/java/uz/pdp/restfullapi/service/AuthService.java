package uz.pdp.restfullapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.entity.enums.RoleName;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqRegister;
import uz.pdp.restfullapi.repository.AwareRepository;
import uz.pdp.restfullapi.repository.RoleRepository;
import uz.pdp.restfullapi.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    ContactService contactService;
    @Autowired
    AwareRepository awareRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse register(ReqRegister request) {
        try {
            if (ageIsValid(request.getBirthDate())) {
                if (request.getPassword().equals(request.getPrePassword())) {
                    if (userRepository.existsByPassportSerialEqualsIgnoreCaseAndPassportNumber(request.getPassportSerial(), request.getPassportNumber())) {
                        return new ApiResponse("Bunday pasport egasi mavjud", false);
                    }
                    User user = new User(
                            request.getFirstName(),
                            request.getLastName(),
                            request.getMiddleName(),
                            request.getPassportSerial(),
                            request.getPassportNumber(),
                            request.getBirthDate(),
                            request.getPhoneNumber(),
                            passwordEncoder.encode(request.getPassword()),
                            contactService.addContact(request.getReqContact()),
                            awareRepository.findById(request.getAwareId()).orElseThrow(() -> new ResourceNotFoundException("getAware")),
                            Collections.singleton(roleRepository.findByRoleName(RoleName.ROLE_USER))
                    );
                    userRepository.save(user);
                    return new ApiResponse("User saqlandi", true);
                } else {
                    return new ApiResponse("Parollar mos emas", false);
                }
            }
            return new ApiResponse("Yoshingiz yetmaydi", false);
        } catch (Exception e) {
            return new ApiResponse("Xatolik", false);
        }
    }


    public boolean ageIsValid(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        Integer client = Integer.valueOf(simpleDateFormat.format(date));
        Integer now = Integer.valueOf(simpleDateFormat.format(new Date()));
        return now - client > 0;
    }

    @Override
    public UserDetails loadUserByUsername(String passport) throws UsernameNotFoundException {
        String[] strings = passport.split("@@");
        return userRepository.findByPassportSerialAndPassportNumber(strings[0], strings[1]).orElseThrow(() -> new UsernameNotFoundException("getUser"));

    }

    public UserDetails getUserById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUser"));
    }


}
