package uz.pdp.restfullapi.service;

import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqCompanyInfo;

import java.util.UUID;

public interface CompanyInfoService {
    ApiResponse addCompanyInfo(ReqCompanyInfo reqCompanyInfo);
    ApiResponse editCompanyInfo(UUID id, ReqCompanyInfo reqCompanyInfo);
    ApiResponse deleteCompanyInfo(UUID id);
}
