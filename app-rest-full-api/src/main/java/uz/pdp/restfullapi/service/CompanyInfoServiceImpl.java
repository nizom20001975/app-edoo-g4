package uz.pdp.restfullapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.restfullapi.entity.AwareCompany;
import uz.pdp.restfullapi.entity.CompanyInfo;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqAwareCompany;
import uz.pdp.restfullapi.payload.ReqCompanyInfo;
import uz.pdp.restfullapi.repository.AwareCompanyRepository;
import uz.pdp.restfullapi.repository.AwareRepository;
import uz.pdp.restfullapi.repository.CompanyInfoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CompanyInfoServiceImpl implements CompanyInfoService {
    @Autowired
    CompanyInfoRepository companyInfoRepository;
    @Autowired
    ContactService contactService;
    @Autowired
    AwareRepository awareRepository;
    @Autowired
    AwareCompanyRepository awareCompanyRepository;

    @Override
    public ApiResponse addCompanyInfo(ReqCompanyInfo request) {
        try {
            CompanyInfo companyInfo = new CompanyInfo();
            companyInfo.setName(request.getName());
            companyInfo.setBrandName(request.getBrandName());
//        Contact contact = contactService.addContact(request.getReqContact());
            companyInfo.setContact(contactService.addContact(request.getReqContact()));
            companyInfo.setDefinition(request.getDefinition());
            CompanyInfo savedCompanyInfo = companyInfoRepository.save(companyInfo);

            //
            List<AwareCompany> awareCompanies = new ArrayList<>();
            for (ReqAwareCompany reqAwareCompany : request.getReqAwareCompanies()) {
                AwareCompany awareCompany = new AwareCompany();
                awareCompany.setAware(awareRepository.findById(reqAwareCompany.getId()).orElseThrow(() -> new ResourceNotFoundException("getAware")));
                awareCompany.setCompanyInfo(savedCompanyInfo);
                awareCompany.setNameUz(reqAwareCompany.getNameUz());
                awareCompany.setNameRu(reqAwareCompany.getNameRu());
                awareCompany.setNameEn(reqAwareCompany.getNameEn());
                awareCompanies.add(awareCompany);
            }
            awareCompanyRepository.saveAll(awareCompanies);
            return new ApiResponse("Company saqlandi", true);
        } catch (Exception e) {
            return new ApiResponse("Saqlashda xatolik", false);
        }


    }

    @Override
    public ApiResponse editCompanyInfo(UUID id, ReqCompanyInfo reqCompanyInfo) {
        return null;
    }

    @Override
    public ApiResponse deleteCompanyInfo(UUID id) {
        return null;
    }
}
