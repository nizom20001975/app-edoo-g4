package uz.pdp.restfullapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.restfullapi.entity.Contact;
import uz.pdp.restfullapi.exception.ExistException;
import uz.pdp.restfullapi.payload.ReqContact;
import uz.pdp.restfullapi.payload.ResContact;
import uz.pdp.restfullapi.repository.ContactRepository;
import uz.pdp.restfullapi.repository.DistrictRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class ContactService {

    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    ContactRepository contactRepository;

    Contact addContact(ReqContact reqContact) {
        if (contactRepository.existsByEmailEqualsIgnoreCase(reqContact.getEmail())) {
            throw new ExistException("Bunday email mavjud");
        }
//        Contact contact = new Contact();
//        contact.setDistrict(districtRepository.findById(reqContact.getDistrictId()).orElseThrow(() -> new ResourceNotFoundException("getDistrict")));
//        contact.setAddress(reqContact.getAddress());
//        contact.setEmail(reqContact.getEmail());
//        contact.setFax(reqContact.getFax());
//        contact.setPhoneNumbers(reqContact.getPhoneNumbers());
        return contactRepository.save(new Contact(
                districtRepository.findById(reqContact.getDistrictId()).orElseThrow(() -> new ResourceNotFoundException("getDistrict")),
                reqContact.getAddress(),
                reqContact.getEmail(),
                reqContact.getFax(),
                reqContact.getPhoneNumbers()
        ));

    }

    Contact editContact(UUID id, ReqContact reqContact) {
        if (contactRepository.existsByEmailEqualsIgnoreCaseAndIdNot(reqContact.getEmail(), id)) {
            throw new ExistException("Bunday email mavjud");
        }
        Optional<Contact> optionalContact = contactRepository.findById(id);
        if (optionalContact.isPresent()) {
            Contact contact = optionalContact.get();
            contact.setDistrict(districtRepository.findById(reqContact.getDistrictId()).orElseThrow(() -> new ResourceNotFoundException("getDistrict")));
            contact.setAddress(reqContact.getAddress());
            contact.setEmail(reqContact.getEmail());
            contact.setFax(reqContact.getFax());
            contact.setPhoneNumbers(reqContact.getPhoneNumbers());
            return contactRepository.save(contact);
        }
        throw new ResourceNotFoundException("Bunday conact topilmadi");
    }


    ResContact getContact(Contact contact) {
        return new ResContact(
                contact.getDistrict(),
                contact.getAddress(),
                contact.getEmail(),
                contact.getFax(),
                contact.getPhoneNumbers()
        );
    }

}
