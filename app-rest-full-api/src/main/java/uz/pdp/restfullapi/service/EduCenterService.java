package uz.pdp.restfullapi.service;

import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqEduCenter;
import uz.pdp.restfullapi.payload.ResEduCenter;
import uz.pdp.restfullapi.payload.ResPageable;

import java.util.UUID;

public interface EduCenterService {
    ApiResponse registerEdu(ReqEduCenter reqEduCenter, User user);

    ApiResponse editEdu(UUID id, ReqEduCenter reqEduCenter, User user);

    ApiResponse changeEnabled(UUID id, User user);

    ResEduCenter getEduCenterOnlyAdminAndDirectorAndManager(UUID id, User user);

    ResPageable getEduCentersForAdmin(int page, int size, User user);

}
