package uz.pdp.restfullapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.restfullapi.entity.EduCenter;
import uz.pdp.restfullapi.entity.User;
import uz.pdp.restfullapi.entity.UserEdu;
import uz.pdp.restfullapi.entity.template.AbsEntity;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqEduCenter;
import uz.pdp.restfullapi.payload.ResEduCenter;
import uz.pdp.restfullapi.payload.ResPageable;
import uz.pdp.restfullapi.repository.*;
import uz.pdp.restfullapi.utils.CommonUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EduCenterServiceImpl implements EduCenterService {
    @Autowired
    ContactService contactService;
    @Autowired
    BusinessTypeRepository businessTypeRepository;
    @Autowired
    EduTypeRepository eduTypeRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    EduCenterRepository eduCenterRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LegalRepository legalRepository;
    @Autowired
    UserEduRepository userEduRepository;
    @Autowired
    TariffRepository tariffRepository;

    @Override
    public ApiResponse registerEdu(ReqEduCenter reqEduCenter, User user) {
        try {
            if (reqEduCenter.getLicenseExpire().after(new Date())) {
                if (reqEduCenter.getFoundedDate().before(new Date())) {
                    if (!eduCenterRepository.existsByBrandEqualsIgnoreCase(reqEduCenter.getBrand())) {
                        if (!eduCenterRepository.existsByNameEqualsIgnoreCase(reqEduCenter.getName())) {
                            if (!eduCenterRepository.existsByTin(reqEduCenter.getTin())) {
                                EduCenter eduCenter = new EduCenter();
                                eduCenter.setName(reqEduCenter.getName());
                                eduCenter.setBrand(reqEduCenter.getBrand());
                                eduCenter.setTin(reqEduCenter.getTin());
                                eduCenter.setAccountNumber(reqEduCenter.getAccountNumber());
                                eduCenter.setFoundedDate(reqEduCenter.getFoundedDate());
                                eduCenter.setContact(contactService.addContact(reqEduCenter.getReqContact()));
                                eduCenter.setBusinessType(businessTypeRepository.findById(reqEduCenter.getBusinessTypeId()).orElseThrow(() -> new ResourceNotFoundException("getBusinessType")));
                                eduCenter.setLicenseNumber(reqEduCenter.getLicenseNumber());
                                eduCenter.setLicenseExpire(reqEduCenter.getLicenseExpire());
                                eduCenter.setEduTypes(eduTypeRepository.findAllById(reqEduCenter.getEduTypesId()));
                                eduCenter.setLogo(attachmentRepository.findById(reqEduCenter.getLogoId()).orElseThrow(() -> new ResourceNotFoundException("getLogo")));
                                eduCenter.setLicense(attachmentRepository.findById(reqEduCenter.getLicenseId()).orElseThrow(() -> new ResourceNotFoundException("getLicense")));
                                eduCenter.setGallery(attachmentRepository.findAllById(reqEduCenter.getGalleryId()));
                                eduCenter.setTariff(tariffRepository.findByPrice(0));
                                EduCenter savedEduCenter = eduCenterRepository.save(eduCenter);
                                //USERNI USEREDU SINI YASASH
                                UserEdu userEdu = new UserEdu();
                                userEdu.setEduCenter(savedEduCenter);
                                userEdu.setLegals(new HashSet<>(legalRepository.findAll()));
                                userEdu.setUser(user);
                                userEduRepository.save(userEdu);
                                return new ApiResponse("Saqlandi", true);
                            }
                            return new ApiResponse("STIR mavjud", false);
                        }
                        return new ApiResponse("name mavjud", false);
                    }
                    return new ApiResponse("brand xato", false);
                }
                return new ApiResponse("tashkil etilgan vaqt xato", false);
            }
            return new ApiResponse("lincence vaqti tuggan", false);
        } catch (Exception e) {
            return new ApiResponse(e.getMessage(), false);
        }
    }

    @Override
    public ApiResponse editEdu(UUID id, ReqEduCenter reqEduCenter, User user) {
        try {
            Optional<UserEdu> userEduOptional = getUserEduOptional(id, user);
            if (userEduOptional.isPresent())
                if (user.getRoles().size() == 1 && userEduOptional.get().getLegals().size() < 5) {
                    return new ApiResponse("Sizda bunday huquq yo'q", false);
                }
            Optional<EduCenter> optionalEduCenter = eduCenterRepository.findByIdAndEnabled(id, true);
            if (optionalEduCenter.isPresent()) {
                if (reqEduCenter.getLicenseExpire().after(new Date())) {
                    if (reqEduCenter.getFoundedDate().before(new Date())) {
                        if (!eduCenterRepository.existsByBrandEqualsIgnoreCaseAndIdNot(reqEduCenter.getBrand(), id)) {
                            if (!eduCenterRepository.existsByNameEqualsIgnoreCaseAndIdNot(reqEduCenter.getName(), id)) {
                                if (!eduCenterRepository.existsByTinAndIdNot(reqEduCenter.getTin(), id)) {
                                    EduCenter eduCenter = optionalEduCenter.get();
                                    eduCenter.setName(reqEduCenter.getName());
                                    eduCenter.setBrand(reqEduCenter.getBrand());
                                    eduCenter.setTin(reqEduCenter.getTin());
                                    eduCenter.setAccountNumber(reqEduCenter.getAccountNumber());
                                    eduCenter.setFoundedDate(reqEduCenter.getFoundedDate());
                                    eduCenter.setContact(contactService.editContact(eduCenter.getContact().getId(),
                                            reqEduCenter.getReqContact()));
                                    eduCenter.setBusinessType(businessTypeRepository.findById(reqEduCenter.getBusinessTypeId()).orElseThrow(() -> new ResourceNotFoundException("getBusinessType")));
                                    eduCenter.setLicenseNumber(reqEduCenter.getLicenseNumber());
                                    eduCenter.setLicenseExpire(reqEduCenter.getLicenseExpire());
                                    eduCenter.setEduTypes(eduTypeRepository.findAllById(reqEduCenter.getEduTypesId()));
                                    eduCenter.setLogo(attachmentRepository.findById(reqEduCenter.getLogoId()).orElseThrow(() -> new ResourceNotFoundException("getLogo")));
                                    eduCenter.setLicense(attachmentRepository.findById(reqEduCenter.getLicenseId()).orElseThrow(() -> new ResourceNotFoundException("getLicense")));
                                    eduCenter.setGallery(attachmentRepository.findAllById(reqEduCenter.getGalleryId()));
                                    eduCenterRepository.save(eduCenter);
                                    return new ApiResponse("Tahrirlandi", true);
                                }
                                return new ApiResponse("STIR mavjud", false);
                            }
                            return new ApiResponse("name mavjud", false);
                        }
                        return new ApiResponse("brand xato", false);
                    }
                    return new ApiResponse("tashkil etilgan vaqt xato", false);
                }
                return new ApiResponse("lincence vaqti tuggan", false);
            }
            return new ApiResponse("Bunday edu yo'q", false);
        } catch (Exception e) {
            return new ApiResponse(e.getMessage(), false);
        }
    }

    @Override
    public ApiResponse changeEnabled(UUID id, User user) {
        Optional<EduCenter> optionalEduCenter = eduCenterRepository.findById(id);
        if (optionalEduCenter.isPresent()) {
            EduCenter eduCenter = optionalEduCenter.get();
            if (user.getRoles().size() > 1) {
                eduCenter.setEnabled(!eduCenter.isEnabled());
            } else {
                UserEdu userEdu = new UserEdu();
                for (UserEdu userEdu1 : user.getUserEduList()) {
                    if (userEdu1.getEduCenter().getId().equals(id)) {
                        userEdu = userEdu1;
                        break;
                    }
                }
                if (userEdu.getLegals().size() > 4) {
                    if (eduCenter.isEnabled()) {
                        eduCenter.setEnabled(false);
                    } else {
                        return new ApiResponse("Bu edu o'chiq allaqachon", false);
                    }
                }
                return new ApiResponse("Bunday huquq yo'q sizda", false);
            }
            eduCenterRepository.save(eduCenter);
            return new ApiResponse(!eduCenter.isEnabled() ? "Edu o'chirildi" : "Edu yoqildi", true);
        }
        return new ApiResponse("Bunday edu yo'q", false);
    }

    @Override
    public ResEduCenter getEduCenterOnlyAdminAndDirectorAndManager(UUID id, User user) {
        Optional<UserEdu> userEduOptional = getUserEduOptional(id, user);
        if (userEduOptional.isPresent() || user.getRoles().size() > 1) {
            if (user.getRoles().size() > 1 || (userEduOptional.isPresent() && userEduOptional.get().getLegals().size() > 3)) {
                EduCenter eduCenter;
                if (userEduOptional.isPresent())
                    eduCenter = userEduOptional.get().getEduCenter();
                else
                    eduCenter = eduCenterRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getEduCenter"));
                return new ResEduCenter(
                        eduCenter.getId(),
                        eduCenter.getName(),
                        eduCenter.getBrand(),
                        eduCenter.getTin(),
                        eduCenter.getAccountNumber(),
                        eduCenter.getFoundedDate(),
                        contactService.getContact(eduCenter.getContact()),
                        eduCenter.getBusinessType(),
                        eduCenter.getLicenseNumber(),
                        eduCenter.getLicenseExpire(),
                        eduCenter.getEduTypes(),
                        eduCenter.getLogo().getId(),
                        eduCenter.getLicense().getId(),
                        eduCenter.getGallery().stream().map(AbsEntity::getId).collect(Collectors.toList()),
                        eduCenter.isEnabled(),
                        null,
                        eduCenter.getTariff()
                );
            }
        }
        return new ResEduCenter();
    }

    @Override
    public ResPageable getEduCentersForAdmin(int page, int size, User user) {
        Page<EduCenter> eduCenterPage = eduCenterRepository.findAll(CommonUtils.getPageable(page, size));
//        List<EduCenter> eduCenterList = eduCenterPage.getContent();
//        List<ResEduCenter> resEduCenterList = new ArrayList<>();
//        for (EduCenter eduCenter : eduCenterList) {
//            ResEduCenter resEduCenter = getEduCenterOnlyAdminAndDirectorAndManager(eduCenter.getId(), user);
//            resEduCenterList.add(resEduCenter);
//        }
        return new ResPageable(
                page,
                size,
                eduCenterPage.getTotalElements(),
                eduCenterPage.getTotalPages(),
                eduCenterPage.getContent().stream().map(eduCenter ->
                        getEduCenterOnlyAdminAndDirectorAndManager(eduCenter.getId(), user))
                        .collect(Collectors.toList())
        );
    }

    public Optional<UserEdu> getUserEduOptional(UUID eduCenterId, User user) {
        for (UserEdu userEdu : user.getUserEduList()) {
            if (userEdu.getEduCenter().getId().equals(eduCenterId)) {
                return Optional.of(userEdu);
            }
        }
        return Optional.empty();
    }
}
