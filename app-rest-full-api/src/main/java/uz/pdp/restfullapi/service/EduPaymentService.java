package uz.pdp.restfullapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.restfullapi.component.EduPaymentListener;
import uz.pdp.restfullapi.entity.EduPayment;
import uz.pdp.restfullapi.entity.enums.OperationEnum;
import uz.pdp.restfullapi.payload.ApiResponse;
import uz.pdp.restfullapi.payload.ReqEduPayment;
import uz.pdp.restfullapi.repository.EduCenterRepository;
import uz.pdp.restfullapi.repository.EduPaymentRepository;
import uz.pdp.restfullapi.repository.PayTypeRepository;

import java.util.UUID;

@Service
public class EduPaymentService {
    @Autowired
    EduPaymentRepository eduPaymentRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    EduCenterRepository eduCenterRepository;
    @Autowired
    EduPaymentListener eduPaymentListener;

    public ApiResponse addEduPayment(ReqEduPayment reqEduPayment) {
        EduPayment savedEduPayment = eduPaymentRepository.save(
                new EduPayment(
                        reqEduPayment.getPaySum(),
                        reqEduPayment.getPayDate(),
                        payTypeRepository.findById(reqEduPayment.getPayTypeId()).orElseThrow(() -> new ResourceNotFoundException("getPayType")),
                        eduCenterRepository.findById(reqEduPayment.getEduCenterId()).orElseThrow(() -> new ResourceNotFoundException("getPayType"))
                )
        );
        eduPaymentListener.saveLog(savedEduPayment, OperationEnum.INSERT);
        return new ApiResponse("Saqlandi", true);
    }

    public ApiResponse editEduPayment(UUID id, ReqEduPayment reqEduPayment) {
        EduPayment eduPayment = eduPaymentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getEduPayment"));
        eduPayment.setEduCenter(eduCenterRepository.findById(reqEduPayment.getEduCenterId()).orElseThrow(() -> new ResourceNotFoundException("getPayType")));
        eduPayment.setPayDate(reqEduPayment.getPayDate());
        eduPayment.setPaySum(reqEduPayment.getPaySum());
        eduPayment.setPayType(payTypeRepository.findById(reqEduPayment.getPayTypeId()).orElseThrow(() -> new ResourceNotFoundException("getPayType")));
        eduPaymentRepository.save(eduPayment);
        return new ApiResponse("Tahrirlandi", true);
    }
}
