package uz.pdp.restfullapi.utils;

public interface AppConstant {
    String DEFAULT_PAGE = "0";
    String DEFAULT_PAGE_SIZE = "10";
    String MAX_PAGE_SIZE = "50";

}
