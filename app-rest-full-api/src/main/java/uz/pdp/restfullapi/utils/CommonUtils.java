package uz.pdp.restfullapi.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import uz.pdp.restfullapi.exception.BadRequestException;

public class CommonUtils {
    public static Pageable getPageable(int page, int size) {
        if (page < 0) {
            throw new BadRequestException("Page soni 0 dan kichik bo'lish mumkin emas");
        }
        if (size > Integer.parseInt(AppConstant.MAX_PAGE_SIZE)) {
            throw new BadRequestException("Size " + AppConstant.MAX_PAGE_SIZE + " dan katta bo'lish mumkin emas");
        }
        return PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
    }
}
