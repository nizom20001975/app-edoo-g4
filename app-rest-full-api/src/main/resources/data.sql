-- insert into role(role_name)values('ROLE_USER');
-- insert into role(role_name)values('ROLE_ADMIN');
insert into role(role_name)values('ROLE_USER'),('ROLE_ADMIN');

insert into region(name_en, name_ru, name_uz)
values ('Tashkent','Ташкент','Toshkent'),('Andijan','Андижан','Andijon');

insert into district(name_en, name_ru, name_uz, region_id)
values ('Yakkasaray','Яккасарай','Yakkasaroy',1),('Xanabad','Ханабад','Xonobod',2);

insert into aware(name_en, name_ru, name_uz, color)
values ('Telegram','Telegram','Telegram','aqua'),
('Facebook','Facebook','Facebook','blue');

insert into business_type(name_en, name_ru, name_uz)
values ('LTD','OOO','MCHJ'),('YATT','ЯТТ','YATT');

insert into edu_type(name_en, name_ru, name_uz)
values('Education','Оброзавние','Ta''lim'),('Profession','Профессия','Kasb-hunar');

-- insert into pay_type(name_en, name_ru, name_uz, color)
-- values ('Click','Click','Click','aqua'),('Payme','Payme','Payme','green');

insert into legal(legal_name)
values
('LEGAL_STUDENT'),
('LEGAL_TEACHER'),
('LEGAL_RECEPTION'),
('LEGAL_MANAGER'),
('LEGAL_DIRECTOR');

insert into tariff(name_en, name_ru, name_uz, active, group_amount, price)
values ('Free','Бесплатно','Bepul',true,4,0),('Standart','Стандарт','Standart',true,8,500000);

insert into duration_type(name_en, name_ru, name_uz)values
('Year','Год','Yil'),
('Month','Месяц','Oy'),
('Week','Неделя','Hafta'),
('Day','День','Kun'),
('Hour','Час','Soat'),
('Minute','Минут','Daqiqa');
